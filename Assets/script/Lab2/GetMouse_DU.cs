using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Teerakarn.GameDev3.chapter2
{
    public class GetMouse_DU : MonoBehaviour
    {
        public float m_MovementStep;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                this.transform.Translate(-m_MovementStep , 0, 0);
            }
            else if (Input.GetMouseButtonUp(1))
            {
                this.transform.Translate(m_MovementStep , 0, 0);
            }
        }
        }
    }
