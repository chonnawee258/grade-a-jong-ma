using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;
using Cinemachine;

public class SwitchCharacter : MonoBehaviour
{
    public ThirdPersonController player1;

    public ThirdPersonController player2;

    public bool player1Active;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            SwitchPlayer();
        }
    }

    public void SwitchPlayer()
    {
        if (player1Active)
        {
            player1.enabled = false;
            player2.enabled = true;
            player1Active = false;
        }
        else
        {
            player1.enabled = true;
            player2.enabled = false;
            player1Active = true;
        }
    }
}
